package DBIx::WithClass; 
# VERSION
# AUTHORITY
# ABSTRACT: DBIx::Class, with more class!
use Modern::Perl;
use Carp;

1;

__END__

# ABSTRACT:

=pod

=head1 DESCRIPTION

In Perl v5.38.0, the first iteration of the new class system will appear. This module is a stake-in-the-ground for
a new module to tie that class system to databases.  NO DEVELOPMENT IS DONE YET, as v5.38.0 is not yet out.

=head1 GOALS OF THE PROJECT

=over 4

=item L<DBIx::Class> MINUS L<Moose>

L<DBIx::Class> is downright brilliant; it ties databases to an ORM that makes a lot of sense in applications. But it has
one semi-large shortcoming: It uses L<Moose>, which is, unfortunately, a bit of a memory hog. Using core code should
help with that.

=item INCORPORATE NEW FEATURES

There are a lot of really great helpers and addons to DBIx::Class that folks have written over its lifetime, not the
least of which are L<fREW|https://metacpan.org/author/FREW>'s wonderful L<DBIx::Class::Candy> and 
L<DBIx::Class::Helpers>. We want to incorporate some of those best-of-breed additions to the module, to lighten the
list of modules you have to load up to use it effectively.

=item MORE DOCUMENTATION

We want to add documentation to make getting started with the module even easier.

=item EASY CHANGEOVER ADOPTION

This module should, at a mimimum, function identically to L<DBIx::Class> from the viewpoint of its consumer; that
will make switching over easier for legacy applications: rework your Result and ResultSet classes to be Perl-native
classes, and all of your reads, writes, and searches to the database Just Work.

=back 

=head1 ASSISTANCE WELCOME

I don't want this to be "just me," or just some tiny group. Here are some specific ways you might be able to help:

=over 4

=item Writing code, once we start doing that

=item Writing documentation

=item Testing, particularly on DB engines other than PostgreSQL/MySQL/MariaDB/SQLite

=item Suggestions of your favorite addons that you think should be included

=back

=head1 ACKNOWLEDGEMENTS

=over 4

=item L<Jason Crome|https://metacpan.org/author/CROMEDOME>, who frequently encourages my mad ideas.

=item L<Tracey Clark|https://metacpan.org/author/TRACEYC>, who's always willing to ask me the hard questions about those ideas.

=back

=cut
